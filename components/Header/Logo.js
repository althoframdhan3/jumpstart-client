import Link from "next/link";
import { useRouter } from "next/router";

import StyledImg from "components/CustomImage";

const Logo = ({ setOpen }) => {
  const router = useRouter();

  return (
    <div style={{ marginRight: "2rem" }}>
      <Link href="/">
        <a onClick={() => setOpen(false)}>
          <StyledImg src="/images/Logo.svg" maxWidth="126px" />
        </a>
      </Link>
    </div>
  );
};

export default Logo;
