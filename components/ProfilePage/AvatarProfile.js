import React from "react";
import { EditIcon } from "@chakra-ui/icons";
import { Avatar } from "@chakra-ui/avatar";
import { Box, Text } from "@chakra-ui/layout";
import { Button, IconButton } from "@chakra-ui/button";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/modal";
import { useDisclosure } from "@chakra-ui/hooks";
import Form from "./EditProfile";

const AvatarProfile = ({ setReload,  name, address }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Avatar size="xl" name={name} mr={4} />
      <Box>
        <Text fontSize="4xl" fontWeight="bold">
          {name}
        </Text>
        <Text fontSize="md" fontWeight="bold" color="gray.500">
          {address}
          <IconButton
            variant="ghost"
            colorScheme="gray"
            aria-label="Edit"
            icon={<EditIcon />}
            onClick={onOpen}
          />
        </Text>
      </Box>

      <Modal
        closeOnOverlayClick={false}
        onClose={onClose}
        size="lg"
        isOpen={isOpen}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Profile</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Form setReload={setReload} onClose={onClose} />
          </ModalBody>
          <ModalFooter></ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default AvatarProfile;
