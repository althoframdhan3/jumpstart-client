import {
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  useBreakpointValue,
} from "@chakra-ui/react";

import JobHistory from "./TabJobHistory";
import AboutMe from "./TabAboutMe";
import JobPost from "./TabJobPost";

const TabsProfile = ({userProfile}) => {
  const tabSize = useBreakpointValue({ base: "sm", sm: "sm", md: "md" });

  return (
    <Tabs isLazy colorScheme="purple" variant="soft-rounded" size={tabSize}>
      <TabList
        pb="2"
        borderBottom="1px"
        borderColor="gray.300"
        overflowX="scroll"
      >
        <Tab _focus={{ outline: "none" }} fontWeight="bold" whiteSpace="nowrap">
          Tentang Saya
        </Tab>
        <Tab _focus={{ outline: "none" }} fontWeight="bold" whiteSpace="nowrap">
          Riwayat Kerja
        </Tab>
        <Tab _focus={{ outline: "none" }} fontWeight="bold" whiteSpace="nowrap">
          Job Post
        </Tab>
      </TabList>

      <TabPanels px={["0", "0", "5"]} pt="10">
        <TabPanel>
          <AboutMe profile={userProfile} />
        </TabPanel>
        <TabPanel>
          <JobHistory jobHistories={userProfile.applications} />
        </TabPanel>
        <TabPanel>
          <JobPost jobPosts={userProfile.jobs} />
        </TabPanel>
      </TabPanels>
    </Tabs>
  );
};

export default TabsProfile;
