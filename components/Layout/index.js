import dynamic from 'next/dynamic'


const Header = dynamic(() => import("components/Header"))
const StyledBody = dynamic(() => import("./styles").then((styles) => styles.StyledBody))

const Layout = ({ children, ...props }) => {
  return (
    <div background="red">
      <Header />
      <StyledBody>{children}</StyledBody>
    </div>
  );
};

export default Layout;
