import { Button } from "@chakra-ui/button";
import { Box, Wrap, WrapItem } from "@chakra-ui/layout";
import axios from "axios";
import { InputLongText, InputText, InputSelect } from "components/Form";
import { FailToast, SuccessToast } from "components/Toast";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { job } from "_api/endpoints";
import { axiosPost } from "components/AxiosRequests";
import authService from "_api/auth";

export const toHumanCase = (posisi) => {
  const strArr = String(posisi).split("_");
  const updatedStr = strArr.map(
    (str) => str.charAt(0).toUpperCase() + str.slice(1),
  );
  return updatedStr.join(" ");
};

const getDateNow = (days=0) => {
  const todaysDate = new Date(); // Gets today's date
  todaysDate.setDate(todaysDate.getDate()+days)

  const year = todaysDate.getFullYear(); // YYYY

  const month = ("0" + (todaysDate.getMonth() + 1)).slice(-2); // MM

  const day = ("0" + todaysDate.getDate()).slice(-2); // DD

  const maxDate = year + "-" + month + "-" + day;

  return maxDate;
};

const JobForm = () => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();

  const onSubmit = (data) => {
    const newData = { ...data, posisi: toHumanCase(data.posisi) };

    axiosPost({
      link: job,
      data: newData,
      toastTitle: "Job Created!",
      toastDesc: "Your Job will published soon",
    })
      .then(() => {
        setTimeout(() => {
          router.push("/cari-kerja");
        }, 1000);
      })
      .catch((err) => {
        if (err.response.status == 403) {
          FailToast({
            title: "Sorry, you must Sign In first",
          });
        }
      });
  };

  return (
    <Box mt="20" maxWidth="2xl" mx="auto">
      <form onSubmit={handleSubmit(onSubmit)}>
        <Wrap direction={["column", "column", "row", "row"]}>
          <WrapItem flex="1">
            <InputText
              name="nama"
              type="text"
              label="Nama Lowongan"
              register={register}
              placeholder={"e.g. Pembuatan Website"}
              required
            />
          </WrapItem>
        </Wrap>
        <InputLongText
          w="full"
          name="description"
          label="Deskripsi Lowongan"
          register={register}
          required
          placeholder="e.g. Saya membutuhkan sebuah website"
        />
        <Wrap
          mb="10"
          justify="space-between"
          w="full"
          direction={["column", "column", "row", "row"]}
        >
          <WrapItem flex="1">
            <InputSelect
              label="Posisi"
              name="posisi"
              register={register}
              items={[
                "Product Design",
                "DevOps Engineer",
                "BackEnd Developer",
                "FrontEnd Developer",
                "Mobile Developer",
              ]}
              required
            />
          </WrapItem>
          <WrapItem flex="1" flexDir="column">
            <InputSelect
              label="Tipe Job"
              name="type"
              register={register}
              items={["Full-Time", "Freelance", "Part-Time"]}
              required
            />
            <InputText
              min={getDateNow()}
              type="date"
              name="expiry"
              label="Kapan ini ditutup?"
              register={register}
              placeholder={""}
              defaultValue={getDateNow(30)}
              required
            />
          </WrapItem>
        </Wrap>
        <Button type="submit" mr="5" variant="solid" colorScheme="purple">
          Submit
        </Button>
        <Button
          variant="outline"
          colorScheme="purple"
          onClick={() => router.back()}
        >
          Cancel
        </Button>
      </form>
    </Box>
  );
};

export default JobForm;
