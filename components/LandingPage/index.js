import { useSelector, useDispatch } from "react-redux";
import { Flex } from "@chakra-ui/react";
import StyledImg from "components/CustomImage";
import LandingText from "./LandingText";

export default function LandingPage() {
  return (
    <Flex
      justifyContent="space-between"
      direction={["column", "column", "column", "row"]}
      alignItems='center'
    >
      <StyledImg src="/images/Hero.svg" maxWidth="573px" />
      <LandingText />
    </Flex>
  );
}
