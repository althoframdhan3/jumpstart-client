import { Alert, AlertIcon } from "@chakra-ui/alert";
import { Button } from "@chakra-ui/button";
import { Checkbox, CheckboxGroup } from "@chakra-ui/checkbox";
import { FormControl, FormLabel } from "@chakra-ui/form-control";
import {
  Input,
  InputGroup,
  InputLeftAddon,
  InputRightElement,
} from "@chakra-ui/input";
import { HStack, Stack } from "@chakra-ui/layout";
import { NumberInput, NumberInputField } from "@chakra-ui/number-input";
import { Radio, RadioGroup } from "@chakra-ui/radio";
import { Select } from "@chakra-ui/select";
import { Tag, TagLabel, TagLeftIcon } from "@chakra-ui/tag";
import { Textarea } from "@chakra-ui/textarea";
import styled from "@emotion/styled";
import React, { useState } from "react";
import { MdError } from "react-icons/md";

const slugify = (label) => String(label).toLowerCase().replace(" ", "_");
const StyledFormControl = styled(FormControl)`
  margin-bottom: 0.8rem;
  width: 100%;
`;

export const InputText = ({
  type,
  label,
  register,
  placeholder,
  required,
  name,
  validation,
  error,
  ...props
}) => (
  <StyledFormControl id={slugify(label)} isRequired={required}>
    <FormLabel fontSize="lg" fontWeight="bold">
      {label}
    </FormLabel>
    <Input
      ref={register({ ...validation })}
      variant="filled"
      name={name}
      {...props}
      type={type}
      placeholder={placeholder}
      isInvalid={error}
    />
    {error && (
      <Tag mt={1} variant="subtle" colorScheme="red">
        <TagLeftIcon as={MdError} />
        <TagLabel>Email invalid</TagLabel>
      </Tag>
    )}
  </StyledFormControl>
);

export const PasswordInput = ({
  name,
  label,
  validation,
  register,
  ...props
}) => {
  const [show, setShow] = useState(false);
  const handleClick = () => setShow(!show);

  return (
    <StyledFormControl {...props} id={slugify(label)} isRequired>
      <FormLabel fontSize="lg" fontWeight="bold">
        {label}
      </FormLabel>
      <InputGroup size="md">
        <Input
          ref={register({ ...validation })}
          name={name}
          pr="4.5rem"
          variant="filled"
          type={show ? "text" : "password"}
          placeholder="Enter password"
          {...register(slugify(label))}
        />
        <InputRightElement width="4.5rem">
          <Button h="1.75rem" size="sm" onClick={handleClick}>
            {show ? "Hide" : "Show"}
          </Button>
        </InputRightElement>
      </InputGroup>
    </StyledFormControl>
  );
};

export const InputUrl = ({
  name,
  label,
  validation,
  register,
  required,
  ...props
}) => (
  <StyledFormControl id={slugify(label)} isRequired={required}>
    <FormLabel fontSize="lg" fontWeight="bold">
      {label}
    </FormLabel>
    <InputGroup>
      <InputLeftAddon children="URL" />
      <Input
        name={name}
        ref={register({ ...validation })}
        variant="filled"
        {...props}
        placeholder="e.g https://www.google.com"
      />
    </InputGroup>
  </StyledFormControl>
);

export const InputLongText = ({
  register,
  label,
  name,
  required,
  placeholder,
  validation,
  ...props
}) => (
  <StyledFormControl id={slugify(label)} isRequired={required}>
    <FormLabel fontSize="lg" fontWeight="bold">
      {label}
    </FormLabel>
    <Textarea
      {...props}
      name={name}
      ref={register({ ...validation })}
      variant="filled"
      placeholder={placeholder}
    />
  </StyledFormControl>
);

export const InputCheckBox = ({
  register,
  label,
  required,
  name,
  items,
  validation,
  ...props
}) => (
  <StyledFormControl id={slugify(label)} isRequired={required}>
    <FormLabel fontSize="lg" fontWeight="bold">
      {label}
    </FormLabel>

    <CheckboxGroup size="lg" colorScheme="green" {...props} defaultValue={[]}>
      <HStack flexDir="column" alignItems="flex-start" pl="4">
        {items.map((item, id) => (
          <Checkbox
            name={name}
            ref={register({ ...validation })}
            key={id}
            marginInlineStart="2"
            colorScheme="purple"
            value={toSnakeCase(item)}
          >
            {item}
          </Checkbox>
        ))}
      </HStack>
    </CheckboxGroup>
  </StyledFormControl>
);

export const toSnakeCase = (type) => {
  return String(type).toLowerCase().replace(/-|\s/, "_");
};

export const InputSelect = ({
  name,
  register,
  validation,
  label,
  required,
  items,
  ...props
}) => {
  return (
    <StyledFormControl isRequired={required}>
      <FormLabel fontSize="lg" fontWeight="bold">
        {label}
      </FormLabel>
      <Select
        {...props}
        name={name}
        ref={register({ ...validation })}
        placeholder="Select option"
      >
        {items.map((item, id) => (
          <option key={id} value={toSnakeCase(item)}>
            {item}
          </option>
        ))}
      </Select>
    </StyledFormControl>
  );
};
