import { Button } from "@chakra-ui/button";
import { Box, Flex, Text } from "@chakra-ui/layout";
import JobFilter from "./JobFilter";
import JobList from "./JobList";
import { MdFilterList } from "react-icons/md";
import { useBoolean } from "@chakra-ui/hooks";
import { useEffect, useState } from "react";
import { toSnakeCase } from "components/Form";
import axios from "axios";
import { getJobWithStatus } from "_api/endpoints";

const WorkBoard = () => {
  const [flag, setFlag] = useBoolean(true);
  const [filterJobs, setJobs] = useState([]);

  const onFilter = async ({ filter = "", job_type = [], skills = [] }) => {
    const response = await axios.get(getJobWithStatus("VERIFIED"));
    const result = await response.data;

    let resultJobs = result;

    resultJobs = resultJobs?.filter((job) =>
      String(job.nama).toLowerCase().includes(filter.toLowerCase()),
    );

    if (job_type.length > 0) {
      resultJobs = resultJobs?.filter((job) =>
        job_type.includes(String(job.type)),
      );
    }

    if (skills.length > 0) {
      resultJobs = resultJobs?.filter((job) =>
        skills.includes(toSnakeCase(job.posisi)),
      );
    }

    setJobs(resultJobs);
  };

  useEffect(() => {
    onFilter({});
  }, []);

  return (
    <Box pos="relative" id="board">
      <Text fontSize="3xl" fontWeight="bold">
        Lowongan yang cocok dengan mu
      </Text>
      <Button
        display={["block", "block", "block", "none"]}
        ml="auto"
        leftIcon={<MdFilterList />}
        colorScheme="gray"
        variant="outline"
        onClick={setFlag.toggle}
      >
        Filters
      </Button>

      <Flex
        justifyContent="space-between"
        direction={[
          "column-reverse",
          "column-reverse",
          "column-reverse",
          "row",
        ]}
        align="flex-start"
      >
        <JobList jobs={filterJobs} />
        {flag && <JobFilter onFilter={onFilter} />}
      </Flex>
    </Box>
  );
};

export default WorkBoard;
