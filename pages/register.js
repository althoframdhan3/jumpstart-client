

import dynamic from "next/dynamic";
const RegisterForm = dynamic(() => import("components/AuthForm/RegisterForm"));

const Register = () => {
  return (
    <>
      <RegisterForm />
    </>
  )
}

export default Register
