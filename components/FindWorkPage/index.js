import WorkBoard from "./WorkBoard";
import WorkLanding from "./WorkLanding";

const WorkPage = () => {
  return (
    <>
      <WorkLanding />
      <WorkBoard />
    </>
  );
};

export default WorkPage;
