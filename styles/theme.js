import "@fontsource/poppins";
import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  styles: {
    global: {
      "html, body": {
        scrollBehavior: "smooth",
        fontFamily: "Poppins",
      },
      "div::-webkit-scrollbar": {
        display: "none",
      },
    },
  },
  components: {
    Toast: {
      baseStyle: {
        fonts: {
          heading: "Poppins",
          body: "Poppins",
        },
      },
    },
    Button: {
      baseStyle: {
        _focus: {
          boxShadow: "none",
        },
      },
    },
  },
});

export default theme;
