import axios from "axios";
import { useEffect, useState } from "react";
import { userProfile } from "_api/endpoints";

const useProfile = () => {
  const [profile, setProfile] = useState("");

  useEffect(() => {
    const response = axios.get(userProfile);
    response
      .then((result) => {
        setProfile(result.data);
      })
      .catch((err) => {
        setProfile(err);
      });
  }, []);

  return profile;
};

export default useProfile;
