import Link from "next/link";

import { Button, Box } from "@chakra-ui/react";

const BtnAuth = ({ setOpen }) => {

  return (
    <Box
      size="md"
      display="flex"
      flexDirection={["column", "column", "column", "row"]}
      w={["100%", "100%", "100%", "initial"]}
    >
      <Link href="/login" passHref>
        <Button onClick={() => setOpen(false)} variant="ghost" colorScheme="purple">
          Masuk
        </Button>
      </Link>
      <Link href="/register" passHref>
        <Button
          onClick={() => setOpen(false)}
          variant="solid"
          colorScheme="purple"
        >
          Daftar
        </Button>
      </Link>
    </Box>
  );
};

export default BtnAuth;
