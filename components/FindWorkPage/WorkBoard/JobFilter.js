import { Button } from "@chakra-ui/button";
import { Box } from "@chakra-ui/layout";
import { InputCheckBox, InputText } from "components/Form";
import { useForm } from "react-hook-form";

const JobFilter = ({onFilter}) => {
  const { register, handleSubmit } = useForm();
  const onSubmit = (data) => {
    onFilter(data);
  };

  return (
    <Box maxW='350px' borderRadius="lg" borderWidth="medium" borderColor="gray.200" p="6">
      <form onSubmit={handleSubmit(onSubmit)}>
        <InputText
          name="filter"
          label="Filter"
          placeholder="Company, Skill, tag..."
          register={register}
        />

        <InputCheckBox
          name="skills"
          label="Skills"
          register={register}
          items={[
            "Product Design",
            "DevOps Engineer",
            "BackEnd Developer",
            "FrontEnd Developer",
            "Mobile Developer",
          ]}
        />
        <InputCheckBox
          name="job_type"
          label="Job type"
          register={register}
          items={["Full-Time", "Freelance", "Part-Time"]}
        />

        <Button type="submit" colorScheme="purple" w="full">
          💻 Find
        </Button>
      </form>
    </Box>
  );
};

export default JobFilter;
