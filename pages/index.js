import dynamic from "next/dynamic";

import { useSelector, useDispatch } from "react-redux";

const LandingPage = dynamic(() => import("components/LandingPage"));

export default function Home() {
  return <LandingPage />;
}
