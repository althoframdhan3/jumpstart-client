import axios from "axios";
import authService from "./auth";

let url;

if (typeof window !== "undefined") {
  url = window.location.origin;
}

axios.interceptors.request.use(
  (config) => {
    const userAuth = authService.loggedIn();
    if (authService.isTokenExist()) {
      const { token } = userAuth;

      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
    }

    return config;
  },
  (error) => Promise.reject(error)
);

const config = {
  base: {
    DOMAIN: url,
  },
  production: {
    API_BASE_URL: "https://jumpstartbe.herokuapp.com",
  },
  development: {
    API_BASE_URL: "http://localhost:8080",
  },
};

export default {
  ...config.base,
  ...config[process.env.NODE_ENV],
};
