import Link from "next/link";

import {
  Avatar,
  AvatarBadge,
  Flex,
  Button,
  Box,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Text,
} from "@chakra-ui/react";

import { useDispatch, useSelector } from "react-redux";
import { getName, logout } from "_redux/authSlice";
import { useRouter } from "next/router";

const Profile = ({ setOpen }) => {
  const dispatch = useDispatch();
  const name = useSelector((state) => state.auth.name);
  const admin = useSelector((state) => state.auth.admin);
  const router = useRouter();

  const onLogout = () => {
    dispatch(logout());
    setOpen(false);
    router.replace("/");
  };

  return (
    <Menu>
      <MenuButton
        _hover={{ cursor: "pointer", bg: "gray.200" }}
        borderRadius="lg"
      >
        <Flex as="span" alignItems="center">
          <Avatar name={name} mr={2} />
          <Box as="span" textAlign="left">
            <p>Halo,</p>
            <Text
              fontWeight="bold"
              as="p"
              isTruncated
              maxW={["100%", "100%", "100%", "120px"]}
            >
              {name}
            </Text>
          </Box>
        </Flex>
      </MenuButton>
      <MenuList>
        {(admin=='true') ? (
          <Link href="/__admin" passHref>
            <MenuItem>Admin Page</MenuItem>
          </Link>
        ) : (
          <Link href="/profile" passHref>
            <MenuItem onClick={() => setOpen(false)}>Profile</MenuItem>
          </Link>
        )}
        <MenuItem onClick={onLogout}>Sign Out</MenuItem>
      </MenuList>
    </Menu>
  );
};

export default Profile;
