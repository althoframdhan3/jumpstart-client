import styled from "styled-components";
import { Box, Flex } from "@chakra-ui/react";

export const Nav = styled(Flex).attrs({
  bg: "white",
  px: [6, 14, 32, 32],
  py: [3, 3, 5, 5],
  width: "100%",
  alignItems: "center",
  justifyContent: "space-between",
  zIndex: 1000,
  minHeight: "88px",
})`
  filter: drop-shadow(0px 0px 5px rgba(0, 0, 0, 0.2));
  position: fixed;
  top: 0;
`;

export const StyledNavLink = styled(Flex).attrs({
  bg: "transparent",
  w: "100%",
  fontSize: ["1.125rem"],
  direction: ["column", "column", "column", "row"],
})`
  white-space: nowrap;
`;

export const StyledWrapper = styled(Flex).attrs(({ open }) => ({
  justifyContent: ["flex-start", "flex-start", "flex-start", "space-between"],
  alignItems: "center",
  direction: ["column", "column", "column", "row"],
  position: ["absolute", "absolute", "absolute", "relative"],
  top: ["88px", "88px", "88px", 0],
  left: 0,
  bg: ["white", "white", "white", "transparent"],
  w: ["100%", "100%", "100%", "initial"],
  px: [6, 14, 32, 0],
  pt: [14, 14, 14, 0],
  h: ["100vh", "100vh", "100vh", "fit-content"],
  display: [!open && "none", !open && "none", !open && "none", "flex"],
}))`
  flex: 1;
`;
