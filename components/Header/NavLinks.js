import Link from "next/link";
import { useRouter } from "next/router";

import { StyledNavLink } from "./styles";

import { Box } from "@chakra-ui/react";

const NavLinks = ({ setOpen }) => {
  const router = useRouter();

  const LINKS = [
    {
      url: "/cari-kerja",
      name: "Cari Kerja",
    },
    {
      url: "/buat-lowongan",
      name: "Buat Lowongan",
    },
  ];

  const color = (url) => (router.asPath === url ? "purple.500" : "gray.500");

  const font = (url) => (router.asPath === url ? "bold" : "light");

  return (
    <StyledNavLink>
      {LINKS.map(({ url, name }) => (
        <Link key={name} href={url}>
          <a onClick={() => setOpen(false)}>
            <Box
              px={[0, 0, 8, 8]}
              color={color(url)}
              fontWeight={font(url)}
              _hover={{ color: "purple.500" }}
              py={[4, 4, 4, 0]}
            >
              {name}
            </Box>
          </a>
        </Link>
      ))}
    </StyledNavLink>
  );
};

export default NavLinks;
