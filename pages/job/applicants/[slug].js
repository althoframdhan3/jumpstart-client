import Applicants from "components/ProfilePage/Applicants";

const JobApplicants = () => {
  return (
    <>
      <Applicants />
    </>
  );
};

export default JobApplicants;

export async function getServerSideProps({ params }) {
  return {
    props: {}, // will be passed to the page component as props
  };
}
