import { Button } from "@chakra-ui/button";
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Link, Text, Wrap, WrapItem } from "@chakra-ui/layout";

import styled from "@emotion/styled";
import { FaGithub } from "react-icons/fa";
import { RiLinkedinFill } from "react-icons/ri";



const generateList = (skills) => (
  String(skills).split(",")
)

const AboutMe = ({profile}) => {

  const skills = generateList(profile.skills)

  const KONTAK = [
    {
      name: "github",
      icon: <FaGithub />,
      color: "teal",
      link: profile.github
    },
    {
      name: "linkedIn",
      color: "linkedin",
      icon: <RiLinkedinFill />,
      link: profile.linkedIn
    },
  ];
  return (
    <>
      <Box>
        <Text fontSize="xl" fontWeight="bold">
          Biografi
        </Text>
        <Text fontSize="md" color="gray.500">
          {profile.biografi}
        </Text>
      </Box>
      <Box>
        <Text fontSize="xl" fontWeight="bold">
          Portfolio
        </Text>
        <Link isExternal href={profile.portofolio} color="gray.500" isExternal>
          {profile.portofolio}
          <ExternalLinkIcon mx="2" />
        </Link>
      </Box>
      <Box>
        <Text fontSize="xl" fontWeight="bold">
          Skills
        </Text>
        <Wrap>
          {skills.map((skill, id) => (
            <WrapItem key={id}>
              <Button colorScheme="gray" size="sm" variant="solid">
                {skill}
              </Button>
            </WrapItem>
          ))}
        </Wrap>
      </Box>
      <Box>
        <Wrap>
          {KONTAK.map((kontak, id) => (
            <WrapItem key={id}>
              <Button
                as="a"
                leftIcon={kontak.icon}
                colorScheme={kontak.color}
                variant="solid"
                size="md"
                target="_blank"
                href={kontak.link}
              >
                {kontak.name}
              </Button>
            </WrapItem>
          ))}
        </Wrap>
      </Box>
    </>
  );
};

const Box = styled.div`
  margin-bottom: 2rem;
`;

export default AboutMe;
