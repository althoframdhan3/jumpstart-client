import JobCard from "components/JobCard";
import JobCardLink from "components/JobCardLink";

const JOB_POSTS = [
  {
    nama: "lowongan 1",
    type: "Part-Time",
    posisi: "Software Engineer",
    pengguna: "GoJek",
    status: "verified",
    slug: "lowongan-1",
  },
  {
    nama: "lowongan 2",
    type: "Full-Time",
    posisi: "Software Engineer",
    pengguna: "GoJek",
    status: "not_verified",
    slug: "lowongan-2",
  },
  {
    nama: "lowongan 3",
    type: "Freelance",
    posisi: "Software Engineer",
    pengguna: "GoJek",
    status: "closed",
    slug: "lowongan-3",
  },
];

const TabJobPost = ({ jobPosts }) => {
  return (
    <>
      {jobPosts.map((job) => (
        <JobCardLink
          key={job.ID}
          name={job.nama}
          role={job.posisi}
          jobType={job.type}
          provider={job.pengguna}
          state={job.status}
          isRecruiter
          slug={job.slug}
          countApplicants={job.job_applicants}
        />
      ))}
    </>
  );
};

export default TabJobPost;
