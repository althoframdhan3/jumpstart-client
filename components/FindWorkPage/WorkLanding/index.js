import { Button } from "@chakra-ui/button";
import { Flex, Text } from "@chakra-ui/layout";
import LandingText from "./LandingText";
import Link from "next/link";
import StyledImg from "components/CustomImage";

const WorkLanding = () => {
  return (
    <Flex
      justifyContent="space-between"
      alignItems="center"
      direction={["column-reverse", "column-reverse", "column-reverse", "row"]}
      mb="40"
    >
      <LandingText />
      <StyledImg src="/images/Work.svg" maxWidth="570" />
    </Flex>
  );
};

export default WorkLanding;
