import JobCard from "components/JobCard";
import useJobsStatus from "hooks/useJobsStatus";

const AcceptedJob = () => {
  const verifiedJob = useJobsStatus("VERIFIED");
  return (
    <>
      {verifiedJob.map((job) => {
        const { id, posisi, type, nama, status } = job;
        return (
          <JobCard
            key={id}
            role={posisi}
            jobType={type}
            name={nama}
            state={status}
          />
        );
      })}
    </>
  );
};

export default AcceptedJob;
