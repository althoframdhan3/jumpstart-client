import axios from "axios";
import { FailToast, SuccessToast } from "./Toast";

export const axiosPost = async ({ link, data, toastTitle, toastDesc }) => {
  try {
    const response = await axios.post(link, data);
    SuccessToast({
      title: toastTitle,
      desc: toastDesc,
    });

    return Promise.resolve(response);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const axiosPut = async ({ link, data, toastTitle, toastDesc }) => {
  try {
    const response = await axios.put(link, data);
    SuccessToast({
      title: toastTitle,
      desc: toastDesc,
    });
    return Promise.resolve(response);
  } catch (err) {
    return Promise.reject(err);
  }
};
