import { IconButton } from "@chakra-ui/button";
import { CloseIcon, HamburgerIcon } from "@chakra-ui/icons";
import { useState } from "react";
import { useSelector } from "react-redux";
import { isLoggedIn } from "_redux/authSlice";

import BtnAuth from "./BtnAuth";
import Logo from "./Logo";
import NavLinks from "./NavLinks";
import Profile from "./Profile";
import { Nav, StyledWrapper } from "./styles";

const Header = () => {
  const [open, setOpen] = useState(false);
  const loggedin = useSelector(isLoggedIn)
  const admin = useSelector((state) => state.auth.admin)

  const switchIcon = open ? <CloseIcon /> : <HamburgerIcon />;

  return (
    <Nav>
      <Logo setOpen={setOpen} />

      <IconButton
        display={["block", "block", "block", "none"]}
        aria-label="Switch Menu"
        size="lg"
        icon={switchIcon}
        onClick={() => setOpen(!open)}
      />

      <StyledWrapper open={open}>
        {!(admin=='true')&&<NavLinks setOpen={setOpen} />}
        {loggedin ? (
          <Profile setOpen={setOpen} />
        ) : (
          <BtnAuth setOpen={setOpen} />
        )}
      </StyledWrapper>
    </Nav>
  );
};

export default Header;
