import { createSlice } from "@reduxjs/toolkit";
import authService from "_api/auth";

const { nama, token, admin } = authService.loggedIn();

const authSlice = createSlice({
  name: "auth",
  initialState: {
    token: token,
    name: nama,
    admin: admin,
  },
  reducers: {
    login: (state, action) => {
      let { token, nama, isAdmin } = action.payload;
      state.token = token;
      state.name = nama;
      state.admin = isAdmin;
      authService.login({
        token: token,
        nama: nama,
        admin: isAdmin,
      });
    },
    logout: (state, action) => {
      state.token = "";
      state.name = "";
      state.admin = false;
      authService.logout();
    },

    setName: (state, action) => {
      state.name = action.payload;
    },
  },
});

export const { setName, logout, login } = authSlice.actions;

export const isLoggedIn = (state) => {
  if (state.auth.token) {
    return true;
  }
  return false;
};

export const getToken = (state) => {
  if (state.auth.token) {
    return state.auth.token;
  }
  return undefined;
};

export const getName = (state) => {
  if (state.auth.name) {
    return state.auth.name;
  }
  return undefined;
};

export default authSlice.reducer;
