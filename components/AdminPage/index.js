import { Box } from "@chakra-ui/layout";
import { useBreakpointValue } from "@chakra-ui/media-query";
import { Tab, TabList, TabPanel, TabPanels, Tabs } from "@chakra-ui/tabs";
import AcceptedJob from "./VerifiedJob";
import PendingJob from "./PendingJob";

const AdminPage = () => {
  const tabSize = useBreakpointValue({ base: "sm", sm: "sm", md: "md" });
  return (
    <Box px={["0", "0", "20"]}>
      <Tabs isLazy colorScheme="purple" variant="soft-rounded" size={tabSize}>
        <TabList
          pb="2"
          borderBottom="1px"
          borderColor="gray.300"
          overflowX="scroll"
        >
          <Tab
            _focus={{ outline: "none" }}
            fontWeight="bold"
            whiteSpace="nowrap"
          >
            Telah diverifikasi
          </Tab>
          <Tab
            _focus={{ outline: "none" }}
            fontWeight="bold"
            whiteSpace="nowrap"
          >
            Belum terverifikasi
          </Tab>
        </TabList>

        <TabPanels px={["0", "0", "5"]} pt="10">
          <TabPanel>
            <AcceptedJob />
          </TabPanel>
          <TabPanel>
            <PendingJob />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Box>
  );
};

export default AdminPage;
