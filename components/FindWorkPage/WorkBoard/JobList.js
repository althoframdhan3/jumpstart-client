import { Box } from "@chakra-ui/layout";
import JobCardLink from "components/JobCardLink";

const JobList = ({ jobs }) => {
  return (
    <Box flex="1" mt={["10", "20"]} mr="5" w="full">
      {jobs.map((job, id) => (
        <JobCardLink
          key={id}
          name={job.nama}
          role={job.posisi}
          jobType={job.type}
          slug={job.slug}
        />
      ))}
    </Box>
  );
};

export default JobList;
