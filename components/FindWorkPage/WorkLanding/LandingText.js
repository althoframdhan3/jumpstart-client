import { Button, ButtonGroup } from "@chakra-ui/button";
import { Flex, Text } from "@chakra-ui/layout";
import Link from "next/link";

const WorkLanding = () => {
  return (
    <Flex
      direction="column"
      justifyContent="space-between"
      alignItems="flex-start"
    >
      <Text fontSize={["3xl", "3xl", "4xl", "5xl"]} fontWeight="bold">
        Mencari pekerjaan jadi lebih simple dan mudah
      </Text>

      <Text fontSize="md" color="gray.400">
        Dipercaya oleh lebih dari 1000+ programmer profesional di Indonesia
      </Text>

      <ButtonGroup spacing="6" colorScheme="purple" size="md">
        <Link href="#board" passHref>
          <Button variant="solid" mt={["10", "20"]}>
            Cari Kerja
          </Button>
        </Link>

        <Link href="/buat-lowongan" passHref>
          <Button mt={["10", "20"]} variant="outline">
            Buat Lowongan
          </Button>
        </Link>
      </ButtonGroup>
    </Flex>
  );
};

export default WorkLanding;
