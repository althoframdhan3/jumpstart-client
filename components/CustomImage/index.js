import styled from "@emotion/styled";

const StyledImg = styled.img`
  object-fit: contain;
  max-width: ${({ maxWidth }) => maxWidth};
  width: 100%;
`;

export default StyledImg;
