import {
  Box,
  Button,
  ButtonGroup,
  Flex,
  Heading,
  LinkBox,
  LinkOverlay,
  Tag,
  Text,
  useBreakpointValue,
  useDisclosure,
} from "@chakra-ui/react";
import NextLink from "next/link";
import Modal from "components/FindWorkPage/modal";
import { toHumanCase } from "components/Job/JobForm";
import { isLoggedIn } from "_redux/authSlice";
import { useSelector } from "react-redux";

const TAG_STATE = {
  verified: {
    color: "green",
    text: "Published",
  },
  closed: {
    color: "red",
    text: "Unpublished",
  },
  not_verified: {
    color: "blackAlpha",
    text: "⏱ Pending",
  },
};

const JobCardLink = ({
  name,
  role,
  jobType,
  isRecruiter = false,
  slug,
  state,
  countApplicants,
}) => {
  const buttonSize = useBreakpointValue({ base: "sm", sm: "sm", md: "md" });
  const { isOpen, onOpen, onClose } = useDisclosure();

  const isAuthenticated = useSelector(isLoggedIn);

  return (
    <LinkBox
      _hover={{ background: `#f1f0f0` }}
      as="article"
      p="5"
      borderBottomWidth="1px"
      rounded="md"
      w="100%"
      mb="2"
    >
      <Flex
        alignItems="center"
        justifyContent="space-between"
        direction={["column", "column", "row"]}
      >
        <Box alignSelf={["flex-start", "flex-start", "inherit", "inherit"]}>
          <Heading size="md" my="2">
            <NextLink
              href={isRecruiter ? `/job/applicants/${slug}` : `/job/${slug}`}
              passHref
            >
              <LinkOverlay >
                <Text maxWidth="400px" isTruncated>{name}</Text>
              </LinkOverlay>
            </NextLink>
            {isRecruiter && (
              <Tag ml="2" colorScheme={TAG_STATE[state].color}>
                {TAG_STATE[state].text}
              </Tag>
            )}
          </Heading>
          <Text>
            {role} &bull; {toHumanCase(jobType)}
          </Text>
        </Box>

        {isRecruiter ? (
          <p>{countApplicants} Applicants</p>
        ) : (
          <>
            <ButtonGroup
              alignSelf={["flex-end", "flex-end", "inherit", "inherit"]}
              mt={[4, 4, 0, 0]}
              colorScheme="gray"
              spacing="3"
              size={buttonSize}
            >
              <NextLink href={`/job/${slug}`} passHref>
                <Button variant="ghost">Detail Job</Button>
              </NextLink>
              {isAuthenticated && (
                <Button onClick={onOpen} colorScheme="green" variant="solid">
                  Apply now
                </Button>
              )}
            </ButtonGroup>
            <Modal slug={slug} isOpen={isOpen} onClose={onClose} />
          </>
        )}
      </Flex>
    </LinkBox>
  );
};

export default JobCardLink;
