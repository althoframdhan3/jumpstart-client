import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
} from "@chakra-ui/react";
import { axiosPost } from "components/AxiosRequests";
import { applyJob } from "_api/endpoints";

const CustomModal = ({ isOpen, onClose, slug }) => {
  const daftar = () => {
    axiosPost({
      link: applyJob(slug),
      toastTitle: "Success"
    });
  };

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Apply Lowongan</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            Apakah kamu yakin ingin melamar pada pekerjaan ini? Pastikan profile
            mu sudah lengkap supaya menarik mata recruiter
          </ModalBody>

          <ModalFooter>
            <Button variant="ghost" onClick={onClose}>
              Tidak
            </Button>
            <Button onClick={daftar} colorScheme="green" ml={3}>
              Yakin
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default CustomModal;
