import axios from 'axios'

const authService = {
  /**
   * Logs a user in, returning a promise with `true` when done
   * @param  {string} email The email of the user
   * @param  {string} password The password of the user
   */
  login(user) {
    
    if (authService.isTokenExist()) {
      return Promise.resolve(true)
    }

    if (typeof window !== 'undefined') {
      localStorage.jumpstart = JSON.stringify(user)
    }

    return Promise.resolve(true)
  },

  /**
   * Logs the current user out
   */
  logout() {
    localStorage.removeItem('jumpstart')
    Reflect.deleteProperty(axios.defaults.headers.common, 'Authorization')
  },

  /**
   * Checks if a user is logged in
   */
  loggedIn() {
    let isUserExist
    
    const userNotExist = {admin:false,token:'',nama: '', email: '' }
    if (typeof window !== 'undefined') {
      isUserExist = Boolean(localStorage.jumpstart)
    }

    return isUserExist ? JSON.parse(localStorage.jumpstart) : userNotExist
  },

  isTokenExist() {
    let isUserExist = false;
    if (typeof window !== 'undefined') {
      isUserExist = Boolean(localStorage.jumpstart)
    }

    return isUserExist
  },

}

export default authService
