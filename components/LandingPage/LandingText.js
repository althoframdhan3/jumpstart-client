import { Button, Text, Box, Flex } from "@chakra-ui/react";
import Link from "next/link";

const LandingText = () => {
  return (
    <Flex
      direction="column"
      justifyContent="space-between"
      alignItems="flex-start"
      maxWidth="550px"
    >
      <Text fontSize="md" fontWeight="bold" color="purple.500">
        Halo, Pengangguran
      </Text>

      <Text fontSize={["4xl","6xl"]} fontWeight="bold">
        Cari Kerja Sekarang juga!
      </Text>

      <Text fontSize="sm" color="gray.400">
        JumpStart merupakan aplikasi media penghubung antara pencari kerja dan
        pemberi lowongan pekerjaan khusus pada bidang IT
      </Text>
      <Link href="/cari-kerja" passHref>
        <Button mt={['10','20']} size="lg" colorScheme="purple">
          Mulai Cari
        </Button>
      </Link>
    </Flex>
  );
};

export default LandingText;
