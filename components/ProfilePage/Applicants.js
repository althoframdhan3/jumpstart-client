import { Button, ButtonGroup, IconButton } from "@chakra-ui/button";
import { useBoolean } from "@chakra-ui/hooks";
import { Badge, Box, Heading, Text } from "@chakra-ui/layout";
import { Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/table";
import axios from "axios";
import { axiosPost } from "components/AxiosRequests";
import { toHumanCase } from "components/Job/JobForm";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { MdCancel, MdCheckCircle } from "react-icons/md";
import { applicantSelect, jobWithSlug } from "_api/endpoints";
import { FailToast } from "components/Toast";

export const USER_APPLICANTS = [
  {
    name: "Andi",
    portfolio: "www.google.com",
  },
  {
    name: "Budi",
    portfolio: "www.google.com",
  },
  {
    name: "Tono",
    portfolio: "www.google.com",
  },
];

const Applicants = () => {
  const router = useRouter();
  const { slug, type, role } = router.query;
  const [flag, setFlag] = useBoolean();
  const [jobDetail, setJobDetail] = useState({});
  const [applicant, setApplicant] = useState({
    accepted: [],
    rejected: [],
  });

  const setAccepted = (id) => {
    setApplicant({
      ...applicant,
      accepted: [...applicant.accepted, id],
    });
  };

  const setRejected = (id) => {
    setApplicant({
      ...applicant,
      rejected: [...applicant.rejected, id],
    });
  };

  const onSubmit = (data) => {
    axiosPost({
      link: applicantSelect,
      data: applicant,
      toastTitle: "Success Update Applicants",
    })
      .then((res) => {
        setFlag.toggle();
      })
      .catch((err) => {
        FailToast({
          title: "Error updating Applicants",
        });
      });
  };

  useEffect(async () => {
    const job = await axios.get(jobWithSlug(slug));
    let jobDetail = await job.data;
    jobDetail = { ...jobDetail, type: toHumanCase(jobDetail.type) };
    setJobDetail(jobDetail);
  }, [flag]);

  const isIncludes = (id) => {
    if (applicant.accepted.includes(id)) {
      return "accepted";
    } else if (applicant.rejected.includes(id)) {
      return "rejected";
    }
    return undefined;
  };

  return (
    <div>
      <Button
        onClick={() => router.back()}
        variant="solid"
        size="md"
        mb="5"
        colorScheme="gray"
      >
        {"< Your Profile"}
      </Button>

      <Heading as="h2" size="3xl" mt="10" mb="5">
        {jobDetail.posisi}
      </Heading>
      <Text fontSize="lg" fontWeight="bold" mb="10" color="purple.600">
        {jobDetail.type} | closed at {"30 Mei 2021"}
      </Text>

      <Box overflowX="scroll">
        <Table colorScheme="purple">
          <Thead>
            <Tr>
              <Th>Nama</Th>
              <Th>Link Portfolio</Th>
              <Th>Reject/Accept</Th>
            </Tr>
          </Thead>
          <Tbody>
            {jobDetail.application?.map((application) => (
              <Tr key={application.id}>
                <Td>{application.user_nama}</Td>
                <Td>{application.user_portfolio}</Td>
                <Td>
                  {application.status == "waiting" ? (
                    isIncludes(application.id) ? (
                      <Badge colorScheme="gray">
                        Added to {isIncludes(application.id)} list
                      </Badge>
                    ) : (
                      <ButtonGroup spacing={5}>
                        <IconButton
                          colorScheme="red"
                          aria-label="Decline"
                          icon={<MdCancel />}
                          onClick={() => setRejected(application.id)}
                        />
                        <IconButton
                          onClick={() => setAccepted(application.id)}
                          colorScheme="green"
                          aria-label="Accept"
                          icon={<MdCheckCircle />}
                        />
                      </ButtonGroup>
                    )
                  ) : (
                    <Badge
                      colorScheme={
                        application.status == "accepted" ? "green" : "red"
                      }
                    >
                      {application.status}
                    </Badge>
                  )}
                </Td>
              </Tr>
            ))}
            {jobDetail.application?.some(
              ({ status }) => status == "waiting",
            ) && (
              <Tr>
                <Td />
                <Td />
                <Td>
                  <Button
                    colorScheme="purple"
                    onClick={() => onSubmit(applicant)}
                  >
                    Save Changes
                  </Button>
                </Td>
              </Tr>
            )}
          </Tbody>
        </Table>
      </Box>
    </div>
  );
};

export default Applicants;
