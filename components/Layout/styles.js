import styled from "styled-components";

import { Box } from "@chakra-ui/react";

export const StyledBody = styled(Box).attrs({
  px: [6, 14, 32],
  pt: [32, 32, 44],
  pb: 24,
  bg: "#f7f7f7",
  minHeight: "100vh",
})``;
