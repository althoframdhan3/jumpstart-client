import axios from "axios";
import { useEffect, useState } from "react";
import { getJobWithStatus } from "_api/endpoints";

const useJobsStatus = (status) => {
  const [jobs, setJobs] = useState([]);

  useEffect(async () => {
    const response = await axios.get(getJobWithStatus(status));
    const result = await response.data;
    setJobs(result);
  }, []);

  return jobs;
};

export default useJobsStatus;
