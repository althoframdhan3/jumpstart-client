import { createStandaloneToast } from "@chakra-ui/react";

const toast = createStandaloneToast();

export const SuccessToast = ({ title, desc }) =>
  toast({
    title: title,
    description: desc,
    status: "success",
    duration: 5000,
    isClosable: true,
    
  });
export const FailToast = ({ title, desc }) =>
  toast({
    title: title,
    description: desc,
    status: "error",
    duration: 5000,
    isClosable: true,
  });
