import axios from "axios";
import { toHumanCase } from "components/Job/JobForm";

import dynamic from "next/dynamic";
import { jobWithSlug } from "_api/endpoints";

const JobPage = dynamic(() => import("components/Job/JobDetail"));

const Job = ({ jobDetail }) => {
  return (
    <>
      <JobPage jobDetail={jobDetail} />
    </>
  );
};

export async function getServerSideProps({ params }) {
  const job = await axios.get(jobWithSlug(params.slug));
  let jobDetail = await job.data;
  jobDetail = {...jobDetail, type: toHumanCase(jobDetail.type)}

  if (!jobDetail) {
    return {
      notFound: true,
    };
  }

  return {
    props: { jobDetail }, // will be passed to the page component as props
  };
}

export default Job;
