import { Button } from "@chakra-ui/button";

import { Flex, Spacer } from "@chakra-ui/layout";
import axios from "axios";
import { axiosPut } from "components/AxiosRequests";

import { InputLongText, InputText, InputUrl } from "components/Form";
import { SuccessToast, FailToast } from "components/Toast";
import useProfile from "hooks/useProfile";

import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { userProfile } from "_api/endpoints";
import { setName } from "_redux/authSlice";

const Form = ({ setReload, onClose }) => {
  const profile = useProfile();
  const { register, handleSubmit } = useForm();
  const dispatch = useDispatch();

  const onSubmit = (data) => {

    axiosPut({
      link: userProfile,
      data: data,
      toastTitle: "Update Profile Success",
      toastDesc: "We've updated your profile",
    })
      .then(() => {
        setReload((prev) => !prev);
        onClose();

        dispatch(setName(data.nama));
      })
      .catch((err) => {
        FailToast({
          title: `Sorry`,
          desc: err,
        });
      });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <InputText
        label="Nama"
        name="nama"
        register={register}
        placeholder="Nama Kau"
        defaultValue={profile.nama}
      />

      <InputLongText
        w="full"
        name="biografi"
        label="Biografi"
        register={register}
        defaultValue={profile.biografi}
        placeholder="e.g. Saya membutuhkan sebuah website"
      />

      <InputText
        label="Alamat"
        name="alamat"
        register={register}
        placeholder="e.g Depok, Indonesia"
        defaultValue={profile.alamat}
      />

      <InputText
        type="text"
        label="Skills"
        name="skills"
        register={register}
        placeholder="e.g SQL, Java, Python"
        defaultValue={profile.skills}
      />

      <InputUrl
        register={register}
        label="Portofolio"
        name="portofolio"
        defaultValue={profile.portofolio}
      />
      <InputUrl
        name=""
        label="GitHub"
        name="github"
        register={register}
        defaultValue={profile.github}
      />
      <InputUrl
        name="linkedIn"
        label="Linkedin"
        label="Linkedin"
        register={register}
        defaultValue={profile.linkedIn}
      />

      <Flex>
        <Spacer />
        <Button onClick={onClose} mr="2">
          Close
        </Button>
        <Button type="submit" colorScheme="green">
          Update & Save
        </Button>
      </Flex>
    </form>
  );
};

export default Form;
