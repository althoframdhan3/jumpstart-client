import AvatarProfile from "./AvatarProfile";

import { Box, Flex } from "@chakra-ui/react";
import ProfileTabs from "./ProfileTabs/TabsProfile";

import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import axios from "axios";
import { userProfile } from "_api/endpoints";

const ProfilePage = () => {
  const [reload, setReload] = useState(false);
  const [profile, setProfile] = useState("");

  useEffect(() => {
    const response = axios.get(userProfile);
    response
      .then((result) => {
        setProfile(result.data);
      })
      .catch((err) => {
        setProfile(err);
      });
  }, [reload]);

  return (
    <>
      <Flex m="auto" w="fit-content" alignItems="center" mb="10">
        <AvatarProfile
          setReload={setReload}
          name={profile?.nama}
          address={profile?.alamat}
        />
      </Flex>
      <Box px={["0", "0", "20"]}>
        <ProfileTabs userProfile={profile} />
      </Box>
    </>
  );
};

export default ProfilePage;
