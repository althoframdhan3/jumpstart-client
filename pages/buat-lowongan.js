import { Button } from "@chakra-ui/button";
import { Heading } from "@chakra-ui/layout";

import dynamic from "next/dynamic";
import Link from "next/link";
import { useSelector } from "react-redux";
import { isLoggedIn } from "_redux/authSlice";
const JobForm = dynamic(() => import("components/Job/JobForm"));

const BuatLowongan = () => {
  const loggedin = useSelector(isLoggedIn);

  return (
    <div style={{ textAlign: "center" }}>
      {loggedin ? (
        <>
          <Heading as="h3" size="3xl">
            Buat Lowongan Pekerjaan
          </Heading>
          <JobForm />
        </>
      ) : (
        <>
          Kamu harus{" "}
          <Link href="/login" passHref>
            <Button variant="ghost" colorScheme="purple">
              Masuk
            </Button>
          </Link>{" "}
          dahulu untuk membuat lowongan, <br />
          jika belum punya akun Kamu bisa
          <Link href="/register" passHref>
            <Button variant="ghost" colorScheme="purple">
              Daftar
            </Button>
          </Link>{" "}
          dahulu
        </>
      )}
    </div>
  );
};

export default BuatLowongan;
