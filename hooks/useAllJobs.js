import axios from "axios";
import { useEffect, useState } from "react";
import { job } from "_api/endpoints";

const useAllJobs = () => {
  const [jobs, setJobs] = useState([]);

  useEffect(async () => {
    const response = await axios.get(job);
    const result = await response.data;
    setJobs(result);
  }, []);

  return jobs;
};

export default useAllJobs;
