import { useRouter } from "next/router";
import dynamic from "next/dynamic";

const ProfilePage = dynamic(() => import("components/ProfilePage"));

const Profile = () => {
  return (
    <>
      <ProfilePage />
    </>
  );
};

export default Profile;
