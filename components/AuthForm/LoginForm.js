import { Button } from "@chakra-ui/button";
import { Box, Heading } from "@chakra-ui/layout";
import axios from "axios";
import { axiosPost } from "components/AxiosRequests";
import { InputText, PasswordInput } from "components/Form";
import { FailToast, SuccessToast } from "components/Toast";
import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { userLogin } from "_api/endpoints";
import { login } from "_redux/authSlice";

const LoginForm = () => {
  const { register, handleSubmit } = useForm();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const onSubmit = (data) => {
    setLoading(true);
    axiosPost({
      link: userLogin,
      data: data,
      toastTitle: "Login Successful",
      toastDesc: "redirecting to Home Page",
    })
      .then((res) => {
        if (res.data.token == null) {
          FailToast({
            title: "Please verify your email address",
          });
        } else {
          dispatch(login(res.data));
          setTimeout(() => {
            if (res.data.isAdmin == "true") {
              router.push("/__admin");
            } else {
              router.push("/");
            }
          }, 1000);
        }
      })
      .catch((err) => {
        FailToast({
          title: err.response.data,
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <Box m="auto" w="100%" maxW="500px">
      <Heading as="h3" fontSize="4xl" mb="10" textAlign="center">
        Login to{" "}
        <Box as="span" color="purple.500">
          Jumpstart
        </Box>
      </Heading>

      <form onSubmit={handleSubmit(onSubmit)}>
        <InputText
          type="email"
          label="Email"
          name="username"
          register={register}
          placeholder="e.g. JohnDoe@gmail.com"
          required
        />
        <PasswordInput name="password" label="Password" register={register} />

        <Button isLoading={loading} type="submit" colorScheme="purple">
          Sign In
        </Button>
      </form>
    </Box>
  );
};

export default LoginForm;
