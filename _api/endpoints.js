import axios from 'axios'
import config from './config'

const API_PREFIX_URL = config.API_BASE_URL;

axios.defaults.baseURL = config.API_BASE_URL

export const job = `${API_PREFIX_URL}/job`
export const jobWithSlug = (slug) => `${API_PREFIX_URL}/job/${slug}`
export const verifyJobWithslug = (slug) => `${API_PREFIX_URL}/job/${slug}/verify`
export const closeJobWithslug = (slug) => `${API_PREFIX_URL}/job/${slug}/close`

export const getJobWithStatus = (status) => `${API_PREFIX_URL}/job?status=${status}`
export const applyJob = (slug) => `${API_PREFIX_URL}/job/${slug}/apply`
export const userProfile = `${API_PREFIX_URL}/user`
export const userLogin = `${API_PREFIX_URL}/login`
export const userRegister = `${API_PREFIX_URL}/register`

export const acceptApplication = (id) => `${API_PREFIX_URL}/application/${id}/accept`
export const rejectApplication = (id) => `${API_PREFIX_URL}/application/${id}/reject`

export const applicantSelect = `${API_PREFIX_URL}/job/process-application`

