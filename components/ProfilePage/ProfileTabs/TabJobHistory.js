import JobCard from "components/JobCard";

const JobHistory = ({ jobHistories }) => {
  return (
    <>
      {jobHistories.map((applicant) => {
        return (
          <JobCard
            key={applicant.id}
            role={applicant.job_posisi}
            jobType={applicant.job_type}
            name={applicant.job_name}
            state={applicant.status}
          />
        );
      })}
    </>
  );
};

export default JobHistory;
