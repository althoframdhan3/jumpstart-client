import { Button } from "@chakra-ui/button";
import { Box, Heading } from "@chakra-ui/layout";
import { axiosPost } from "components/AxiosRequests";
import { InputSelect, InputText, PasswordInput } from "components/Form";
import { FailToast } from "components/Toast";
import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { userRegister } from "_api/endpoints";

const RegisterForm = () => {
  const { register, handleSubmit, errors } = useForm();
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  const onSubmit = (data) => {
    setLoading(true);
    axiosPost({
      link: userRegister,
      data: data,
      toastTitle: "Registration Successful",
      toastDesc: "Please do check your email, for activation",
    })
      .then(() => {
        setTimeout(() => {
          router.push("/login");
        }, 3000);
      })
      .catch((err) => {
        FailToast({
          title: err.response.data
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <Box m="auto" w="100%" maxW="500px">
      <Heading as="h3" fontSize="4xl" mb="10" textAlign="center">
        Register to{" "}
        <Box as="span" color="purple.500">
          Jumpstart
        </Box>
      </Heading>

      <form onSubmit={handleSubmit(onSubmit)}>
        <InputText
          name="nama"
          type="text"
          label="Nama"
          register={register}
          placeholder=""
          required
        />
        <InputText
          validation={{
            pattern:
              /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
          }}
          error={errors.username}
          name="username"
          type="email"
          label="Email"
          register={register}
          placeholder="e.g. JohnDoe@gmail.com"
          required
        />
        <PasswordInput name="password" label="Password" register={register} />

        <Button isLoading={loading} type="submit" colorScheme="purple">
          Create Account
        </Button>
      </form>
    </Box>
  );
};

export default RegisterForm;
