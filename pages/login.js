

import dynamic from "next/dynamic";
const LoginForm = dynamic(() => import("components/AuthForm/LoginForm"));

const Login = () => {
  
  return (
    <>
      <LoginForm />
    </>
  )
}

export default Login
