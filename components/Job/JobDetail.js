import { Button } from "@chakra-ui/button";
import { Heading, Text } from "@chakra-ui/layout";
import { axiosPost } from "components/AxiosRequests";

import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { applyJob } from "_api/endpoints";
import { isLoggedIn } from "_redux/authSlice";

const JobPage = ({ jobDetail }) => {
  const router = useRouter();
  const { slug, type, provider } = router.query;
  const isAuthenticated = useSelector(isLoggedIn);

  const daftar = () => {
    axiosPost({
      link: applyJob(slug),
      toastTitle: "Success",
    });
  };

  return (
    <div>
      <Button
        onClick={() => router.back()}
        variant="solid"
        size="md"
        mb="5"
        colorScheme="gray"
      >
        {"< Semua Lowongan"}
      </Button>

      <Heading as="h2" size="3xl" mt="10" mb="5">
        {jobDetail.nama}
      </Heading>
      <Text fontSize="lg" fontWeight="bold" mb="10" color="purple.600">
        {jobDetail.posisi} ({jobDetail.type} | closed at{" "}
        {new Date(jobDetail.expiry).toDateString()})
      </Text>
      <Text w="100%" maxW="800px" whiteSpace="pre-wrap">
        {jobDetail.description}
      </Text>
      {isAuthenticated && (
        <Button mt="5" onClick={daftar} colorScheme="green" variant="solid">
          Apply Now
        </Button>
      )}
    </div>
  );
};

export default JobPage;
