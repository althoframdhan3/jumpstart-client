import {
  Badge,
  Box,
  Heading,
  IconButton,
  LinkBox,
  Tag,
  Text,
  Wrap,
} from "@chakra-ui/react";
import { axiosPost } from "components/AxiosRequests";
import { toHumanCase } from "components/Job/JobForm";
import { useState } from "react";
import { MdCancel, MdCheckCircle } from "react-icons/md";
import { verifyJobWithslug, closeJobWithslug } from "_api/endpoints";

const JobCard = ({ role, jobType, name, state, isAdmin = false, slug }) => {
  const [isClicked, setIsClicked] = useState(false);

  const verifiedJob = (jobSlug) => {
    setIsClicked(true);
    axiosPost({
      link: verifyJobWithslug(jobSlug),
      toastTitle: "job verified",
      toastDesc: jobSlug + " was verified",
    }).catch((err) => {});
  };

  const closeJob = (jobSlug) => {
    setIsClicked(true);
    axiosPost({
      link: closeJobWithslug(jobSlug),
      toastTitle: "job closed",
      toastDesc: jobSlug + " was closed",
    }).catch((err) => {});
  };

  return (
    <LinkBox
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      as="article"
      p="5"
      borderWidth="1px"
      rounded="md"
      w="100%"
      mb="2"
    >
      <Box>
        <Heading size="md" my="2">
          <Box as="span" mr={3}>
            {name}
          </Box>
          {state && (
            <Tag colorScheme={TAG_STATE[state].color}>
              {TAG_STATE[state].text}
            </Tag>
          )}
        </Heading>
        <Text>
          {role} &bull; {toHumanCase(jobType)}
        </Text>
      </Box>

      {isAdmin && (
        <Wrap spacing="5">
          {isClicked ? (
            <Badge colorScheme="gray">Verified</Badge>
          ) : (
            <>
              <IconButton
                colorScheme="red"
                aria-label="Decline"
                icon={<MdCancel />}
                onClick={() => closeJob(slug)}
              />
              <IconButton
                colorScheme="green"
                aria-label="Accept"
                icon={<MdCheckCircle />}
                onClick={() => verifiedJob(slug)}
              />
            </>
          )}
        </Wrap>
      )}
    </LinkBox>
  );
};

export default JobCard;

const TAG_STATE = {
  accepted: {
    color: "green",
    text: "Accepted",
  },
  rejected: {
    color: "red",
    text: "Rejected",
  },
  waiting: {
    color: "blackAlpha",
    text: "Waiting",
  },

  verified: {
    color: "green",
    text: "Published",
  },
  closed: {
    color: "red",
    text: "Unpublished",
  },
  not_verified: {
    color: "blackAlpha",
    text: "On Checking",
  },
};
