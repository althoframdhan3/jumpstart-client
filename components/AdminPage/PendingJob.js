import JobCard from "components/JobCard";
import useJobsStatus from "hooks/useJobsStatus";

const PendingJob = () => {
  const pendingJob = useJobsStatus("NOT_VERIFIED");

  return (
    <>
      {pendingJob.map((job) => {
        const { id, posisi, type, nama, slug } = job;
        return (
          <JobCard
            key={id}
            role={posisi}
            jobType={type}
            name={nama}
            slug={slug}
            isAdmin
          />
        );
      })}
    </>
  );
};

export default PendingJob;
