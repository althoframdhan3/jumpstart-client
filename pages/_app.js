import Head from "next/head";
import dynamic from "next/dynamic";

import store from "_redux/store";
import theme from "styles/theme";

import { Provider } from "react-redux";
import { ChakraProvider } from "@chakra-ui/react";


const Layout = dynamic(() => import("components/Layout"));

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Head>
        <title>Jumpstart</title>
      </Head>
      <ChakraProvider theme={theme}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </ChakraProvider>
    </Provider>
  );
}

export default MyApp;
